﻿using MailKit.Net.Smtp;
using MimeKit;

namespace CompNetworksTask
{
    public class SMTPService
    {
        const string subject = "Welcome";
        const string testUserEmail = "semenchik.polina@mail.ru";
        const string messageText = @"Hello,

We are happy to see you with us.

-- FoodDelivery";

        public void SendMessage(string to = testUserEmail, string subject = subject, string messageBody = messageText)
        {
            // Creare message
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress(SMTPServerService.from));
            message.To.Add(new MailboxAddress(to));
            message.Subject = subject;

            message.Body = new TextPart("plain")
            {
                Text = messageBody
            };
            
            
            // Send the message
            using (var client = new SmtpClient())
            {
                // For demo-purposes, accept all SSL certificates 
                client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                client.Connect(SMTPServerService.smtpServer, SMTPServerService.port, false);
                client.Authenticate(SMTPServerService.authFrom, SMTPServerService.authPassword);

                client.Send(message);
                client.Disconnect(true);
            }
       }

    }
}
