﻿using System.Net.Http;

namespace CompNetworksTask
{
    public static class HttpClientHelper
    {
        // Send a get request, display content of response
        public static async void Get(string url, string apiKey = "")
        {
            if (apiKey.Length > 0)
            {
                url = GetAuthURL(url, apiKey);
            }

            // Use HttpClient
            using (HttpClient client = new HttpClient())
            {
                using (HttpResponseMessage response = await client.GetAsync(url))
                {
                    using (HttpContent content = response.Content)
                    {
                        // Read the string
                        string result = await content.ReadAsStringAsync();

                        // Display the result
                        MessageHandler.Output(result);
                    }
                }
            }
        }

        // Get url with apiKey
        private static string GetAuthURL(string url, string apiKey)
        {
            int keyValuePosition = url.IndexOf("key=") + "key=".Length;
            string authUrl = url.Insert(keyValuePosition, apiKey);
            return authUrl;
        }
    }
}
