﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Net.Http;
using System.Threading.Tasks;

namespace CompNetworksTask
{
    class Program
    {
        const string namedayUSAUrl = "https://api.abalin.net/get/today?country=us";
        const string holidayUSAUrl = "https://holidayapi.com/v1/holidays?key=&country=US&year=2018&pretty";
       
        static void Main(string[] args)
        {
            Console.WriteLine("Computer networks task");
            Console.WriteLine("Type help for a list of commands");

            while (true)
            {
                string commandStr = Convert.ToString(Console.ReadLine());

                if (commandStr.Length < 1)
                {
                    continue;
                }

                string[] commandArr = commandStr.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                switch (commandArr[0])
                {
                    case "help":
                        DisplayCommandList();
                        break;

                    case "email":
                        SMTPService sMTPServer = new SMTPService();
                                               
                        try
                        {
                            if (commandArr.Length == 4)
                            {
                                var emailAddressAttribute = new EmailAddressAttribute();
                                if (emailAddressAttribute.IsValid(commandArr[1]))
                                {
                                    sMTPServer.SendMessage(commandArr[1], commandArr[2], commandArr[3]);
                                }
                                else
                                {
                                    throw new Exception("Invalid email adress");
                                }
                            }
                            else
                            {
                                sMTPServer.SendMessage();
                            }
                            Console.WriteLine("Sent successfully");
                        }
                        catch(Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        break;
                    case "getnameday":
                        GetNamedaysUSA();
                        break;
                    case "getholi":
                        GetHolidaysUSA(APIKeys.holidayApiKey);
                        break;
                    case "exit":
                        return;

                    default:
                        Console.WriteLine("'{0}' is not recognized as a command", commandStr);
                        break;
                }
            }

        }

        // Display list of commands
        private static void DisplayCommandList()
        {
            Console.WriteLine("help     display list of commands");
            Console.WriteLine("email    send a test message");
            Console.WriteLine("      [TO] [SUBJECT] [MESSAGEBODY]    send a message to the ");
            Console.WriteLine("                 email with the message body, adding the subject");
            Console.WriteLine("getnameday     get json of usa namedays for today");
            Console.WriteLine("getholi      get json of usa holidays of 2019");
            Console.WriteLine("exit     exit the application");
        }

        // Get json of usa holidays of 2019
        static async void GetHolidaysUSA(string apiKey)
        {
            await Task.Run(() => HttpClientHelper.Get(holidayUSAUrl, apiKey));
            Console.WriteLine("Getting USA holidays...");
        }

        // Get json of usa namedays for today"
        static async void GetNamedaysUSA()
        {
            await Task.Run(()=> HttpClientHelper.Get(namedayUSAUrl));
            Console.WriteLine("Getting namedays...");
        }
                              
    }
}
